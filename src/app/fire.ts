export class Fire {
    id: number;
    lat: number;
    lon: number;
    temp: number;
    area: number;
    report_date: Date;
}
