export class Recipient {
    id: number;
    email: string;
    phone: string;
    active: boolean;
}
