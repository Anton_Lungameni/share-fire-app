import { Injectable } from '@angular/core';
import {Fire} from './fire';
import {FIRES} from './test_fires';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FireService {

  constructor() { }

  getFires(): Observable<Fire[]> {
    return of(FIRES);
  }
}
