import { Component, OnInit } from '@angular/core';
import { Fire } from '../fire';
import { FireService } from '../fire.service';

@Component({
  selector: 'app-fire-notifications',
  templateUrl: './fire-notifications.component.html',
  styleUrls: ['./fire-notifications.component.css']
})
export class FireNotificationsComponent implements OnInit {

  selectedFire: Fire;
  fires: Fire[];

  constructor(private fireService: FireService) { }

  ngOnInit() {
    this.getFires();
  }

  onSelect(fire: Fire): void {
    this.selectedFire = fire;
  }

  getFires(): void {
    this.fireService.getFires()
      .subscribe(fires => this.fires = fires);
  }

}
