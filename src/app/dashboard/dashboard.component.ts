import { Component, OnInit, HostListener } from '@angular/core';
import { FireService } from '../fire.service';
import { Fire } from '../fire';

import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM.js';
import {defaults as defaultControls, FullScreen} from 'ol/control.js';
import Point from 'ol/geom/Point.js';
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import {Fill, RegularShape, Stroke, Style} from 'ol/style.js';
import Feature from 'ol/Feature.js';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  fires: Fire[];
  features: Feature[];
  latitude = -26.000;
  longitude = 23.906;
  stroke = new Stroke({color: 'black', width: 2});
  fill = new Fill({color: 'red'});
  styles: any;
  feature: any;
  feature_1: any;
  map: any;


  constructor(private fireService: FireService) {
    this.getFires();
    this.features = new Array<Feature>();
  }

  getFires(): void {
    this.fireService.getFires()
      .subscribe(fires => this.fires = fires);
  }

  ngOnInit() {
    this.styles = {
      'star': new Style({
        image: new RegularShape({
          fill: this.fill,
          stroke: this.stroke,
          points: 5,
          radius: 10,
          radius2: 4,
          angle: 0
        })
      })
    };

    this.fires.forEach(fire => {
      const x = new Feature(new Point([fire.lon, fire.lat]));
      x.setStyle(this.styles['star']);
      this.features.push(x);
    });

    // this.feature = new Feature(new Point([ 18.5271, -33.6443]));
    // this.feature.setStyle(this.styles['star']);

    // this.feature_1 = new Feature(new Point([ 17.26, -22.7]));
    // this.feature_1.setStyle(this.styles['star']);

    const source = new VectorSource({
      features: this.features
    });

    const vectorLayer = new VectorLayer({
      source: source
    });

    this.map = new Map({
      target: 'map',
      controls: defaultControls().extend([
        new FullScreen()
      ]),
      layers: [
        new TileLayer({
          source: new OSM()
        }), vectorLayer
      ],
      view: new View({
        projection: 'EPSG:4326',
        center: [  this.longitude, this.latitude],
        zoom: 4
      })
    });
  }

@HostListener('window:resize', ['$event'])
onresize(event) {
  this.map.updateSize();
}

}
