import { Fire } from './fire';

export const FIRES: Fire[] = [
    {id: 1, lat: -22.465, lon: 17.698, temp: 45, area: 50, report_date: new Date('2018-10-20T16:10:12')},
    {id: 2, lat: -23.465, lon: 16.698, temp: 33, area: 30, report_date: new Date('2018-10-21T16:20:12')},
    {id: 3, lat: -27.732, lon: 30.740, temp: 53, area: 20, report_date: new Date('2018-10-23T16:20:12')},
];
